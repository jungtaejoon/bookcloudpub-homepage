import React from 'react';
import {Button} from "react-bootstrap";

import './Hero.css'

function Hero(props) {

    const {imageURL, height} = props;

    const heroStyle = {
        backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(${imageURL})`,
        height: `${height}`,
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundAttachment: 'fixed',
        backgroundSize: 'cover',
        position: 'relative'
    };

    return (
        <div className="hero-image" style={heroStyle}>
            <div className="hero-text">
                <h1>I am John Doe</h1>
                <p>And I'm a Photographer</p>
                <Button>Hire me</Button>
            </div>
        </div>
    )
}

export default Hero;