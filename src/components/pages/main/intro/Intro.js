import React, {Component, Fragment} from 'react';
import Hero from "../../commonComponent/hero/Hero";
import {Button, Image} from "react-bootstrap";

import './Intro.css'

class Intro extends Component{
    constructor(props) {
        super(props);
        this.myInput = React.createRef();
        this.state = {
            x: 0,
            y: 0
        }
    }
    test = (e) => {
        const {winWidth, winHeight} = this.state;
        this.setState({
            x: e.screenX - winWidth / 2,
            y: e.screenY - winHeight / 2
        });
    };

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
        this.contentsHeight = document.getElementById('intro-contents').offsetHeight;

    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions = () => {
        this.setState({ winWidth: window.innerWidth, winHeight: window.innerHeight });
    };

    render() {
        const {x, y, winWidth, winHeight} = this.state;
        const style = {
            transform: `translate(${x/30}px, ${y/30}px)`,
        };
        const contentsStyle = {
            width: '600px',
            top: `${((winHeight / 2) - (this.contentsHeight / 2))}px`,
            left: `${((winWidth / 2) - 300)}px`,
            transform: `translate(${-x/40}px, ${-y/40}px)`
        };
        return (
            <div id='bg' onMouseMove={this.test}>
                <img style={style} src='' data-src="https://images.pexels.com/photos/266098/pexels-photo-266098.jpeg?cs=srgb&dl=adorable-baby-beautiful-266098.jpg&fm=jpg" />
                <div id='intro-contents' style={contentsStyle}>
                    <h1>I am John Doe</h1>
                    <p>And I'm a Photographer</p>
                    <Button>Hire me</Button>
                </div>
            </div>
        );
    }

}

// render() {
//     const {x, y} = this.state;
//     const style = {
//         transform: `translate(${x/30}px, ${y/30}px)`,
//     };
//     return (
//         <div id='bg' onMouseMove={this.test}>
//             <img style={style} src="" data-src="https://images.pexels.com/photos/266098/pexels-photo-266098.jpeg?cs=srgb&dl=adorable-baby-beautiful-266098.jpg&fm=jpg" />
//             <div className='intro-contents'>
//                 <h1>I am John Doe</h1>
//                 <p>And I'm a Photographer</p>
//                 <Button>Hire me</Button>
//             </div>
//         </div>
//     );
// }
// function Intro(props) {
//     let x = 0;
//     let y = 0;
//
//     var ctrans = 'translate(' + x + 'px, '+y+'px)';
//     var css = {
//         transform: ctrans
//     }
//     const test = (e) => {
//         x = e.screenX;
//         y = e.screenY;
//         console.log(ctrans, x, y)
//     };
//
//     return (
//             <div style={css} onMouseMove={test} className='intro-container'>
//                 <div className='intro-contents'>
//                     <h1>I am John Doe</h1>
//                     <p>And I'm a Photographer</p>
//                     <Button>Hire me</Button>
//                 </div>
//             </div>
//     )
// }

export default Intro;