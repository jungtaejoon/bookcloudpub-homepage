import React, {Fragment} from 'react'
import Hero from "../commonComponent/hero/Hero";
import {Container} from "react-bootstrap";
import ReactFullpage from "@fullpage/react-fullpage";
import Intro from "./intro/Intro";
import FrontDoor from "./frontDoor/FrontDoor";

const s = {
    fontSize: '300px'
};

const Home = (props) => {
    return (
        <ReactFullpage
            navigation
            render={({ state, fullpageApi }) => {
                console.log(fullpageApi)
                return (
                    <ReactFullpage.Wrapper>
                        <div className="section">
                            <FrontDoor/>
                        </div>
                        <div className="section">
                            <Intro/>
                        </div>
                        <div className="section">
                            <p>Section 2</p>
                        </div>
                    </ReactFullpage.Wrapper>
                );
            }}
        />
    );
};

export default Home