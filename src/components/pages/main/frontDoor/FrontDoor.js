import React from 'react';
import Hero from "../../commonComponent/hero/Hero";
import {Button, Image} from "react-bootstrap";

import './FrontDoor.css'

function FrontDoor(props) {
    return (
        <div className='front-door-container'>
            <div className='front-door-contents'>
                <h1>I am John Doe</h1>
                <p>And I'm a Photographer</p>
                <Button>Hire me</Button>
            </div>
        </div>
    )
}

export default FrontDoor;