import React, { Component } from 'react';
import {inject, observer} from "mobx-react";

@inject('contentsStore')
@observer
class ContentsFrame extends Component {
    render() {
        const {contentsStore} = this.props;
        return (
            <div>
                {contentsStore.page}
                <button onClick={contentsStore.inc}>+</button>
                <button onClick={contentsStore.setPage.bind(null, 10)}>10</button>
            </div>
        );
    }
}

export default ContentsFrame;