import React, {Component, Fragment} from 'react';
import NavigationBar from './navigationBar/NavigationBar';
import ContentsFrame from "./ContentsFrame";
import MainPageFrame from './pages/main/MainPageFrame'
import BlogPageFrame from './pages/blog/BlogPageFrame'
import {Route} from "react-router-dom";

export default class PageFrame extends Component {
    render() {
        return (
            <Fragment>
                <NavigationBar/>
                <Route exact path="/" component={MainPageFrame}/>
                <Route path="/blog" component={BlogPageFrame}/>
            </Fragment>
        );
    }
}