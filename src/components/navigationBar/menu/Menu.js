import React from 'react';
import {NavLink} from "react-router-dom";

const menuStyle = {
    fontWeight: '600'
};

function Menu(props) {
    return (
        <NavLink className='nav-link' style={menuStyle} exact to='blog'>블로그</NavLink>
    )
}

export default Menu;