import React, {Component} from 'react';
import {Nav, Navbar} from "react-bootstrap";
import {NavLink} from 'react-router-dom';

import './NavigationBar.css'
import Login from "./login/Login";
import Menu from "./menu/Menu";

const backgroundState = ['', 'light', 'dark'];

const brandStyle = {
    fontWeight: '600',
    fontSize: '24px'
};

const navbarStyle = {
    transition: '0.5s'
};

export default class NavigationBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isTop: true
        }
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll = (event) => {
        const scrollTop = event.srcElement.scrollingElement.scrollTop;
        if (scrollTop <= 0) {
            if(!this.state.isTop) this.setState({
                isTop: true
            })
        } else if (this.state.isTop) {
            this.setState({
                isTop: false
            })
        }
    };

    render() {
        const {isTop} = this.state;
        return (
            <Navbar collapseOnSelect style={navbarStyle} expand="lg" fixed="top" bg={isTop ? backgroundState[0] : backgroundState[1]} variant={isTop ? backgroundState[2] : backgroundState[1]}>
                <NavLink className='navbar-brand' style={brandStyle} exact to='/'>책구름</NavLink>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                        <Menu/>
                    </Nav>
                    <Nav>
                        <Login/>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}