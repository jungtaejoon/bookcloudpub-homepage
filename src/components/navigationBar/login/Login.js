import React from 'react';
import {Button, Form, NavDropdown} from "react-bootstrap";

import './Login.css';

const loginInputStyle = {
    width: '350px'
};

function Login(props) {
    return (
        <NavDropdown alignRight title="Login" id="collasible-nav-dropdown">
            <Form className='mx-3 my-2 w-auto'>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email"  style={loginInputStyle} />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" />
                </Form.Group>
                <Form.Group controlId="formBasicChecbox">
                    <Form.Check type="checkbox" label="Remember me" />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Login
                </Button>
            </Form>
        </NavDropdown>
    )
}

export default Login;