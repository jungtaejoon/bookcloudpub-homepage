import React, {Component} from 'react';
import './App.css';
import PageFrame from './components/PageFrame';
import {BrowserRouter} from "react-router-dom";
import ScrollToTop from "./components/ScrollToTop";

class App extends Component {
  render() {
    return (
        <BrowserRouter>
            <ScrollToTop>
                <PageFrame/>
            </ScrollToTop>
        </BrowserRouter>


    );
  }
}

export default App;
