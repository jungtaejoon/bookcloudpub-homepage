import {action, observable} from "mobx";

export default class ContentsStore {
    @observable page = 0;
    @observable contents = '';

    @action setPage = (num) => {
        this.page = num;
        console.log(this.page);
    };

    @action inc = () => {
        this.page++;
    };
}
